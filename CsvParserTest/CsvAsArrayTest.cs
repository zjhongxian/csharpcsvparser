﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qifun.CsvParser;
using Qifun.CsvParserUtil;
namespace CsvParserUtilTest
{
    [TestClass]
    public class CsvAsArrayTest1
    {
        [TestMethod]
        public void TestGet()
        {
            var csvReader = CsvReader.Factory(new FileInfo("../../test.csv"));
            var csvArray = new CsvAsArray(csvReader);
            string[] line1 = { "a", "b", "c,c", "d" };
            string[] line2 = { "1", "2", "3\n3", "4" };
            Assert.AreEqual(line1[1], csvArray[0, 1]);
            Assert.AreEqual(line1[2], csvArray[0, 2]);
            Assert.AreEqual(line2[2], csvArray[1, 2]);
            csvReader.Close();
        }
        [TestMethod]
        public void TestGetError()
        {
            var csvReader = CsvReader.Factory(new FileInfo("../../test.csv"));
            var csvArray = new CsvAsArray(csvReader);
            string tryNow = null;
            string[] line1 = { "a", "b", "c,c", "d" };
            string[] line2 = { "1", "2", "3\n3", "4" };
            try
            {
               tryNow = csvArray[2, 2];
            }
            catch(Exception exce)
            {
                Assert.AreEqual(exce.Message, "out of range!");
            }
            try
            {
                tryNow = csvArray[-1, 2];
            }
            catch (Exception exce)
            {
                Assert.AreEqual(exce.Message, "out of range!");
            }
            try
            {
                tryNow = csvArray[20, 0];
            }
            catch (Exception exce)
            {
                Assert.AreEqual(exce.Message, "out of range!");
            }
            csvReader.Close();
        }
    }
    
}
