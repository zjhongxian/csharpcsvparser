﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Qifun.CsvParser
{
    internal class LineReader
    {
        readonly private TextReader reader;
        internal LineReader(TextReader reader)
        {
            this.reader = reader;
        }

        internal string ReadLine()
        {
            var stringBuilder = new StringBuilder();
            while (true)
            {
                var current = reader.Read();

                if (current == -1)
                {
                    if (stringBuilder.Length == 0)
                        return null;
                    else
                        break;
                }
                else if (current == 65279)
                {
                    continue;
                }

                stringBuilder.Append(Convert.ToChar(current));

                if (current == '\n' || current == '\u2028' || current == '\u2029' || current == '\u0085')
                {
                    break;
                }

                if (current == '\r')
                {
                    var next = reader.Peek();
                    if (next == -1)
                    {
                        break;
                    }
                    else if (next == '\n')
                    {
                        current = reader.Read();
                        stringBuilder.Append('\n');
                    }
                    break;
                }

            }
            return stringBuilder.ToString();
        }

        internal void Close()
        {
            reader.Close();
        }
    }
}