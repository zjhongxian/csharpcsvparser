﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Qifun.CsvParser;
using System.Collections;
namespace Qifun.CsvParserUtil
{
    public class CsvAsArray
    {
        private string[,] csvArray;
        private int highLength = 0;
        private int wideLength = 0;
        public CsvAsArray(CsvReader csvReader)
        {
            ArrayList saveNow = new ArrayList();
            var enumerator = csvReader.GetEnumerator();
            while (enumerator.MoveNext())
            {
                ArrayList lineList = new ArrayList();
                highLength++;
                wideLength = Math.Max(wideLength, enumerator.Current.Length);
                lineList.Clear();
                for(int i=0;i<wideLength;i++)
                {
                    lineList.Add(enumerator.Current[i]);
                }
                saveNow.Add(lineList);
            }
            csvArray = new string[highLength,wideLength];
            highLength = 0;
            foreach(ArrayList line in saveNow)
            {
                int j=0;
                foreach(string num in line)
                {
                    csvArray[highLength, j] = num;
                    j++;
                }
                highLength++;
            }
        }
        /// <summary>
        /// 按照数组方式获得值
        /// </summary>
        /// <param name="a">第a行</param>
        /// <param name="b">第b列</param>
        /// <returns>值</returns>
        public String this[int a, int b]
        {
            get
            {
                if ((a >= 0 && a < highLength) && (b >= 0 && b < wideLength)) return csvArray[a,b];
                else throw new Exception("out of range!");
            }
        }
        
    }
}
