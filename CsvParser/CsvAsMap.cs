﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Qifun.CsvParser
{
    public class CsvAsMap
    {
        private Dictionary<String, Dictionary<String, String>> csvMap = new Dictionary<String, Dictionary<String, String>>();
        private String[] listName;
        private Boolean[] flag;
        public CsvAsMap(CsvReader csvReader)
        {
            var enumerator = csvReader.GetEnumerator();
            enumerator.MoveNext();
            listName = enumerator.Current;
            HashSet<string> ans = checkListName(listName);
            if(ans.Count!=0)
            {
                string exception = "列名重复，重复的列名有:";
                foreach(var list in ans)
                {
                    exception += " " + list;
                }
                throw new Exception(exception);
            }
            flag = new Boolean[listName.Length];
            if (listName == null) return;
            for (int i = 0; i < listName.Length;i++)
            {
                string getNow = listName[i].Replace(" ", "");
                if (getNow.Length == 0) flag[i] = false;
                else flag[i] = true;
            }
            while (enumerator.MoveNext())
            {
                if (enumerator.Current == null) break;
                string getNow = enumerator.Current[0].Replace(" ","");
                if(getNow.Length > 0)csvMap.Add(enumerator.Current[0], GetCsvLine(enumerator.Current));
            }

        }
        private Dictionary<String, String> GetCsvLine(String[] line)
        {
            Dictionary<String, String> lineMap = new Dictionary<string,string>();
            for(int i =0 ;i<listName.Length;i++)    
            {
                if (flag[i])
                {
                    if (line.Length > i) lineMap.Add(listName[i], line[i]);
                    else lineMap.Add(listName[i], "");
                }
            }
            return lineMap;
        }

        /// <summary>
        /// 获得值
        /// </summary>
        /// <param name="row">行标号</param>
        /// <param name="list">列标号</param>
        /// <returns>值</returns>
        public string this[string row,string list]
        {
            get
            {
                try
                {
                    return csvMap[row][list];
                }
                catch(KeyNotFoundException )
                {
                    throw new Exception("参数输入错误，无法找到相应的值！");
                }
            }
        }

        /// <summary>
        /// 获得这个Map的迭代器
        /// </summary>
        /// <returns> 迭代器 </returns>

        public IEnumerator<KeyValuePair<string, Dictionary<string, string>>> GetEnumerator()
        {
            return csvMap.GetEnumerator();
        }
        private HashSet<string> checkListName(string[] listName)
        {
            HashSet<string> set = new HashSet<string>();
            HashSet<string> ans = new HashSet<string>();
            ans.Clear();
            set.Clear();
            for (int i = 0; i < listName.Length;i++ )
            {
                if (!set.Add(listName[i])) ans.Add(listName[i]);
            }
            return ans;
        }
    }
}
